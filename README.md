# iOS MobilePay

# Requirements

Xcode 11.7 or above

Cocoapods (Optional)

# How to install

#### Cocoapods

-   Add `'pod MobilePay','~> 3.0'` to your Podfile under a target app
-   Run `pod install`
-   Refer to the Example project if in doubt

# How to use

## Steps to use the SDK:

2. Import framework in Swift source code: **_import MobilePay_**

4. Initialize SDK by creating the instance of  `MobilePayViewController` as in the example below:

```swift
let ssoLogin = SSOLogin(
    							customerId: "customer_id_string", 
    							integrationKey: "integration_key_string", 
    							email: "valid email address"
 							 )   

let initialViewController = MobilePayViewController(
     														login: ssoLogin
  													)
```

- `login` - object encapsulating login information.
  -   customerId: id of the customer in a third party scope
  -   integrationKey: A unique client key for the environment that is aquired from the Tappit
  -   email: Customer email address.
  

5. Present the controller in any way you like for example by pushing it to the UINavigationController.

## Additional deep link button on home screen

SDK supports one custom deep link button that will appear at the bottom of the home screen and would perform a required action:

``` swift
let deepLink = DeepLink(url: <url link to somewhere in your application>)
let initialViewController = MobilePayViewController(
     														<............>
  															deepLink: deepLink
  													)
```

Implementing deep link an additional button will appear at the bottom of the home screen and when pressed - will execute navigation to the deep link url provided. For customizing the image of the deep link please refer to the branding section of the images override below.

## Application permissions

You have to add the following permissions to the application`s plist file:`

* `NSCameraUsageDescription` - Allow the camera access for scanning credit cards when adding a payment method
* `NSFaceIDUsageDescription` - Allow face time authentication access for a more convenient login

``` swift
<key>NSCameraUsageDescription</key>
<string>Please allow camera access to scan the bank cards</string>
<key>NSFaceIDUsageDescription</key>
<string>Authorize with Face ID</string>
```



## Testing on different environments

SDK supports different test environment:

* `Sandbox`
*  `Test`
*  `Staging`
*  `Production`

 Default environment is Production. To test on a different environment you have to pass an additional parameter to `SSOLogin` object:

```swift
   let ssoLogin = SSOLogin(
                  	<..............>
      							environment: .Test,
                  )
```

## Providing additional integration ids for third party services

SDK allows integrating with different external services:

* Ticketmaster
* Tickets.com
* Paciolan
* AXS
* Ticketsocket
* LoyaltyId
* SkiData

To take advantage of any of those integrations please provide an additional array of `IntegrationData` objects to the `SSOLogin`:

```swift
let ticketMasterData = IntegrationData(
  												integrationType: 0, 
  												valueType: 1, 
  												value: "token", 
  												isGlobal: true
											 )
let ssoLogin = SSOLogin(
  								<..............>
                  integrationData: [integrationData])
```

The following are all the supported values for each item:

1. integrationType: TicketMaster = 0, Tickets.Com = 1, Paciolan = 2 AXS = 3 TicketSocket = 4 LoyaltyId = 5 SkiData = 6
2. valueType: User ID = 0, API TOKEN = 1
3. value: actual value of the data
4. isGlobal: true or false depending if the value is unique globally

## Branding assets

### Branding images override

There are several images that application can override to customize SDK. Please add any of the following images to the default assets catalog of the application to override them from the SDK:

* `tmw_navigation_logo` - main app logo
* `tmw_deep_link` - if your app requires deep inking from the SDK into other place. Will add additional button with this image on the main home screen. (Use alongside optional initialization parameter on the `MobilePayViewController` mentioned above to enable this feature)

Provide the images named as described inside application's `xcassets` catalog. Refer to the Example project for reference.

### How to override colors

SDK supports these colors overrides:

-   `tmw_back_indicator` - back button tint color
-   `tmw_biometric_indicator` - biometric image tint color
-    `tmw_navigation_background` - navigation bar background tint color
-   `tmw_onboarding_background_text` - text that is on onboarding background tint color
-   `tmw_onboarding_background` - onboarding background tint color
-   `tmw_onboarding_button_background` - buttons which are on the onboarding background tint color
-   `tmw_onboarding_button_text` - onboarding buttons text tint color
-   `tmw_page_header_text` - headers of the page tint color
-   `tmw_primary_background_text` - primary object background text tint color
-   `tmw_primary_background` - primary object background tint color
-   `tmw_secondary_background_text` - secondary object background text tint color
-   `tmw_secondary_background` - secondary object background tint color
-   `tmw_pay_button_background` - pay button on the main home screen backgroung color
-   `tmw_pay_button_text` - pay button on the main home screen text color
-   `tmw_pay_button_border` - pay button on the main home screen border color
- `tmw_home_button_background` - home button (any other button than pay button) on the main home screen backgroung color
- `tmw_home_button_text` - home button (any other button than pay button) on the main home screen text color
- `tmw_home_button_border` - home button (any other button than pay button) on the main home screen border color
- `tmw_primary_button_background` - any general button following the main home screen, that usually  has a possitive effect, background color
- `tmw_primary_button_text` - any general button following the main home screen, that usually  has a possitive effect, text color
- `tmw_primary_button_border` - any general button following the main home screen, that usually  has a possitive effect, border color
- `tmw_secondary_button_background` - any 2nd button where there is more than just one button per screen following the main home screen, that usually  has a negative effect, background color
- `tmw_secondary_button_text` - any 2nd button where there is more than just one button per screen following the main home screen, that usually  has a negative effect, text color
- `tmw_secondary_button_border` - any 2nd button where there is more than just one button per screen following the main home screen, that usually  has a negative effect, border color
- `tmw_custom_unselected_tip_button_background` - tip percentage button default background color
- `tmw_custom_unselected_tip_button_text` - tip percentage button default text color
- `tmw_custom_unselected_tip_button_border` - tip percentage button default border color
- `tmw_custom_selected_tip_button_background` - selected tip percentage button background color
- `tmw_custom_selected_tip_button_text` - selected tip percentage button text color
- `tmw_custom_selected_tip_button_border` - selected tip percentage button border color
- `tmw_unselected_benefit_button_background` - benefits (like giveaway tokens or giftcards) activation button background color
- `tmw_unselected_benefit_button_text` - benefits (like giveaway tokens or giftcards) activation button text color
- `tmw_unselected_benefit_button_border` - benefits (like giveaway tokens or giftcards) activation button border color
- `tmw_selected_benefit_button_background`  - activated benefits (like giveaway tokens or giftcards) activation button background color
- `tmw_selected_benefit_button_text` - activated benefits (like giveaway tokens or giftcards) activation button text color
- `tmw_selected_benefit_button_border` - activated benefits (like giveaway tokens or giftcards) activation button border color
- `tmw_redeemable_campaign_card_text` - redeemable campaign table row text color
- `tmw_redeemable_campaign_card_background` - redeemable campaign table row background color
- `tmw_redeemable_campaign_card_border` - redeemable campaign table row border color
- `tmw_redeemed_campaign_card_text` - redeemed campaign table row text color
- `tmw_redeemed_campaign_card_background` - redeemed campaign table row background color
- `tmw_redeemed_campaign_card_border` - redeemed campaign table row border color
- `tmw_redeemable_campaign_button_background` - redeemable campaign status button background color
- `tmw_redeemable_campaign_button_text` - redeemable campaign status button text color
- `tmw_redeemable_campaign_button_border` - redeemable campaign status button border color
- `tmw_redeemed_campaign_button_background` - redeemed campaign status button background color
- `tmw_redeemed_campaign_button_text` - redeemed campaign status button text color
- `tmw_redeemed_campaign_button_border` - redeemed campaign status button border color
- `tmw_transaction_card_background` - transactions table row background color
- `tmw_transaction_card_text` - transactions table row text color
- `tmw_transaction_card_border` - transactions table row border color
- `tmw_notification_badge_background` - notification badge to indicate updates background color
- `tmw_notification_badge_text` - notification badge to indicate updates text color

To override any combination of those put each color inside the main application`s assets catalog as a separate color. Refer to the Example project for an example. Override all the colors as needed.

### Client settings 

All of the client settings (terms and conditions links, help link, brand name etc.) are downloaded from the backend and can be changed through the admin panel. 

### Fonts

To override the fonts, first the application should have a custom dictionary entry in its plist file with the key named `tmw_tappit_config`. Then please use the following values to override supported fonts:

- `tmw_fonts` - values of the fonts to be overriden. Supported fonts in the SDK are:
  - `bold` - name of the family of the general custom bold font
  - `normal` - name of the family of the general custom normal font
  - `benefit_status_button` - benefits (like giveaway tokens or giftcards) status button fonts
  - `campaign_status_button` - campaign status button font
  - `custom_tip_button` - custom tip percentage button font
  - `home_button` - home button (any other button on the home screen rather than pay button) font
  - `pay_button` - pay button on the home screen font
  - `primary_button` - any general button after home screen font
  - `secondary_button` - any 2nd button along side the primary button font

Example plist entry:

```xml
<key>tmw_tappit_config</key>
	<dict>
		<key>tmw_fonts</key>
		<dict>
			<key>benefit_status_button</key>
			<string>HelveticaNeue-Bold</string>
			<key>bold</key>
			<string>StonecutterSans-Bold</string>
			<key>campaign_status_button</key>
			<string>HelveticaNeue-Bold</string>
			<key>custom_tip_button</key>
			<string>HelveticaNeue-Bold</string>
			<key>home_button</key>
			<string>HelveticaNeue-Bold</string>
			<key>normal</key>
			<string>Karu</string>
			<key>pay_button</key>
			<string>StonecutterSans-Bold</string>
			<key>primary_button</key>
			<string>HelveticaNeue-Bold</string>
			<key>secondary_button</key>
			<string>HelveticaNeue-Bold</string>
		</dict>
	</dict>
```

If using custom fonts remember to provide entry of the fonts provided by the application in the plist file as well:

```xml
<key>UIAppFonts</key>
	<array>
		<string>bold.ttf</string>
		<string>normal.ttf</string>
	</array>
```

