// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2.4 (swiftlang-1103.0.32.9 clang-1103.0.32.53)
// swift-module-flags: -target x86_64-apple-ios11.4-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MobilePay
import AVFoundation
import CommonCrypto
import CoreData
import CoreGraphics
import Foundation
import LocalAuthentication
import PassKit
import Swift
import SystemConfiguration
import UIKit
import UserNotifications
import VideoToolbox
import Vision
import WebKit
public protocol JWT {
  var header: [Swift.String : Any] { get }
  var body: [Swift.String : Any] { get }
  var signature: Swift.String? { get }
  var string: Swift.String { get }
  var expiresAt: Foundation.Date? { get }
  var issuer: Swift.String? { get }
  var subject: Swift.String? { get }
  var audience: [Swift.String]? { get }
  var issuedAt: Foundation.Date? { get }
  var notBefore: Foundation.Date? { get }
  var identifier: Swift.String? { get }
  var expired: Swift.Bool { get }
}
extension JWT {
  public func claim(name: Swift.String) -> MobilePay.Claim
}
extension UINavigationController : PassKit.PKPaymentAuthorizationViewControllerDelegate {
  @objc dynamic public func paymentAuthorizationViewControllerDidFinish(_ controller: PassKit.PKPaymentAuthorizationViewController)
  @objc dynamic public func paymentAuthorizationViewController(_ controller: PassKit.PKPaymentAuthorizationViewController, didAuthorizePayment payment: PassKit.PKPayment, handler completion: @escaping (PassKit.PKPaymentAuthorizationResult) -> Swift.Void)
}
public struct DeepLink {
  public init(url: Foundation.URL)
}
public struct CreditCard {
  public var number: Swift.String?
  public var name: Swift.String?
  public var expireDate: Foundation.DateComponents?
}
@objc @_hasMissingDesignatedInitializers public class MobilePayViewController : UIKit.UIViewController {
  public init(login: MobilePay.SSOLogin, deepLink: MobilePay.DeepLink? = nil)
  @objc override dynamic public func viewDidAppear(_ animated: Swift.Bool)
  @objc override dynamic public func viewWillAppear(_ animated: Swift.Bool)
  @objc override dynamic public func viewDidLoad()
  public func willHandle(url: Foundation.URL) -> Swift.Bool
  @objc deinit
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
}
@_hasMissingDesignatedInitializers @objc(A0JWT) public class _JWT : ObjectiveC.NSObject {
  @objc public var header: [Swift.String : Any] {
    @objc get
  }
  @objc public var body: [Swift.String : Any] {
    @objc get
  }
  @objc public var signature: Swift.String? {
    @objc get
  }
  @objc public var expiresAt: Foundation.Date? {
    @objc get
  }
  @objc public var expired: Swift.Bool {
    @objc get
  }
  @objc public class func decode(jwt jwtValue: Swift.String) throws -> MobilePay._JWT
  @objc deinit
  @objc override dynamic public init()
}
public struct IDTokenValidation {
  public let issuer: Swift.String
  public let audience: Swift.String
  public init(issuer: Swift.String, audience: Swift.String)
  public func validate(_ jwt: MobilePay.JWT, nonce: Swift.String? = nil) -> MobilePay.ValidationError?
}
public enum DecodeError : Foundation.LocalizedError {
  case invalidBase64Url(Swift.String)
  case invalidJSON(Swift.String)
  case invalidPartCount(Swift.String, Swift.Int)
  public var localizedDescription: Swift.String {
    get
  }
}
public func decode(jwt: Swift.String) throws -> MobilePay.JWT
public struct Claim {
  public var rawValue: Any? {
    get
  }
  public var string: Swift.String? {
    get
  }
  public var boolean: Swift.Bool? {
    get
  }
  public var double: Swift.Double? {
    get
  }
  public var integer: Swift.Int? {
    get
  }
  public var date: Foundation.Date? {
    get
  }
  public var array: [Swift.String]? {
    get
  }
}
public struct CreditCardScannerError : Foundation.LocalizedError {
  public enum Kind {
    case cameraSetup, photoProcessing, authorizationDenied, capture
    public static func == (a: MobilePay.CreditCardScannerError.Kind, b: MobilePay.CreditCardScannerError.Kind) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public var kind: MobilePay.CreditCardScannerError.Kind
  public var underlyingError: Swift.Error?
  public var errorDescription: Swift.String? {
    get
  }
}
@available(iOS 13, *)
public protocol CreditCardScannerViewControllerDelegate : AnyObject {
  func creditCardScannerViewControllerDidCancel(_ viewController: MobilePay.CreditCardScannerViewController)
  func creditCardScannerViewController(_ viewController: MobilePay.CreditCardScannerViewController, didErrorWith error: MobilePay.CreditCardScannerError)
  func creditCardScannerViewController(_ viewController: MobilePay.CreditCardScannerViewController, didFinishWith card: MobilePay.CreditCard)
}
@available(iOS 13, *)
extension CreditCardScannerViewControllerDelegate where Self : UIKit.UIViewController {
  public func creditCardScannerViewControllerDidCancel(_ viewController: MobilePay.CreditCardScannerViewController)
}
@objc @available(iOS 13, *)
open class CreditCardScannerViewController : UIKit.UIViewController {
  public var titleLabelText: Swift.String
  public var subtitleLabelText: Swift.String
  public var cancelButtonTitleText: Swift.String
  public var cancelButtonTitleTextColor: UIKit.UIColor
  public var labelTextColor: UIKit.UIColor
  public var textBackgroundColor: UIKit.UIColor
  public var cameraViewCreditCardFrameStrokeColor: UIKit.UIColor
  public var cameraViewMaskLayerColor: UIKit.UIColor
  public var cameraViewMaskAlpha: CoreGraphics.CGFloat
  public init(delegate: MobilePay.CreditCardScannerViewControllerDelegate)
  @available(*, unavailable)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc override dynamic open func viewDidLoad()
  @objc override dynamic open func viewDidLayoutSubviews()
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc deinit
}
public enum ValidationError : Swift.Error {
  case invalidClaim(Swift.String)
  case expired
  case nonce
}
public struct Regex : Swift.ExpressibleByStringLiteral {
  public typealias StringLiteralType = Swift.String
  public init(stringLiteral value: MobilePay.Regex.StringLiteralType)
  public init(_ string: Swift.String)
  public func matches(in string: Swift.String) -> [Swift.String]
  public func hasMatch(in string: Swift.String) -> Swift.Bool
  public func firstMatch(in string: Swift.String) -> Swift.String?
  public func replacingOccurrences(in string: Swift.String, with replacement: Swift.String = "") -> Swift.String?
  public func captures(in string: Swift.String) -> [Swift.String]
  public typealias ExtendedGraphemeClusterLiteralType = MobilePay.Regex.StringLiteralType
  public typealias UnicodeScalarLiteralType = MobilePay.Regex.StringLiteralType
}
infix operator =~ : DefaultPrecedence
infix operator !~ : DefaultPrecedence
extension Regex {
  public static func =~ (string: Swift.String, regex: MobilePay.Regex) -> Swift.Bool
  public static func =~ (regex: MobilePay.Regex, string: Swift.String) -> Swift.Bool
  public static func !~ (string: Swift.String, regex: MobilePay.Regex) -> Swift.Bool
  public static func !~ (regex: MobilePay.Regex, string: Swift.String) -> Swift.Bool
}
public enum MobilePayEnvironment : Swift.String, Swift.CaseIterable {
  case Sandbox, Test, Staging, Production
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
  public init?(rawValue: Swift.String)
  public typealias AllCases = [MobilePay.MobilePayEnvironment]
  public static var allCases: [MobilePay.MobilePayEnvironment] {
    get
  }
}
extension IntegrationData : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct IntegrationData {
  public let integrationType: Swift.Int
  public let valueType: Swift.Int
  public let value: Swift.String
  public let isGlobal: Swift.Bool
  public init(integrationType: Swift.Int, valueType: Swift.Int, value: Swift.String, isGlobal: Swift.Bool)
}
public struct SSOLogin {
  public let customerId: Swift.String
  public var integrationData: [MobilePay.IntegrationData]
  public let email: Swift.String
  public let environment: MobilePay.MobilePayEnvironment
  public init(customerId: Swift.String, integrationKey: Swift.String, email: Swift.String, environment: MobilePay.MobilePayEnvironment = .Production, integrationData: [MobilePay.IntegrationData]? = nil)
}
extension UIView {
  @objc dynamic public func badge(text badgeText: Swift.String?)
  public func badge(text badgeText: Swift.String?, appearance: MobilePay.BadgeAppearance)
}
extension UIBarButtonItem {
  @objc dynamic public func badge(text: Swift.String?)
  public func badge(text badgeText: Swift.String?, appearance: MobilePay.BadgeAppearance = .default)
}
public struct BadgeAppearance {
  public static var `default`: MobilePay.BadgeAppearance {
    get
  }
  public var font: UIKit.UIFont
  public var textAlignment: UIKit.NSTextAlignment
  public var borderColor: UIKit.UIColor
  public var borderWidth: CoreGraphics.CGFloat
  public var allowShadow: Swift.Bool
  public var backgroundColor: UIKit.UIColor
  public var textColor: UIKit.UIColor
  public var animate: Swift.Bool
  public var duration: Foundation.TimeInterval
  public var distanceFromCenterY: CoreGraphics.CGFloat?
  public var distanceFromCenterX: CoreGraphics.CGFloat?
  public var radius: CoreGraphics.CGFloat?
  public init(font: UIKit.UIFont = .systemFont(ofSize: 12), textAlignment: UIKit.NSTextAlignment = .center, borderColor: UIKit.UIColor = .clear, borderWidth: CoreGraphics.CGFloat = 0.0, allowShadow: Swift.Bool = false, backgroundColor: UIKit.UIColor = .red, textColor: UIKit.UIColor = .white, animate: Swift.Bool = true, duration: Foundation.TimeInterval = 0.2, distanceFromCenterY: CoreGraphics.CGFloat? = nil, distanceFromCenterX: CoreGraphics.CGFloat? = nil, radius: CoreGraphics.CGFloat? = nil)
}
@_inheritsConvenienceInitializers @objc(ManagedBalance) public class ManagedBalance : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedBalance {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedBalance>
  @objc @NSManaged dynamic public var currency: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var preloadAmount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var complimentaryBalances: Foundation.NSSet? {
    @objc get
    @objc set
  }
}
extension ManagedBalance {
  @objc(addComplimentaryBalancesObject:) @NSManaged dynamic public func addToComplimentaryBalances(_ value: MobilePay.ManagedComplimentaryBalance)
  @objc(removeComplimentaryBalancesObject:) @NSManaged dynamic public func removeFromComplimentaryBalances(_ value: MobilePay.ManagedComplimentaryBalance)
  @objc(addComplimentaryBalances:) @NSManaged dynamic public func addToComplimentaryBalances(_ values: Foundation.NSSet)
  @objc(removeComplimentaryBalances:) @NSManaged dynamic public func removeFromComplimentaryBalances(_ values: Foundation.NSSet)
}
@_inheritsConvenienceInitializers @objc(ManagedCampaign) public class ManagedCampaign : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedCampaign {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedCampaign>
  @objc @NSManaged dynamic public var benefit_configuration: Foundation.Data? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var benefit_display_text: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var benefit_expiration_date: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var benefit_id: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var benefit_type: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var campaign_description: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var campaign_end_date: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var campaign_id: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var campaign_name: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var campaign_progress_value: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var campaign_start_date: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var complimentary_balance_redeemed_amount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var day_configuration: Foundation.Data? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var is_give_away_token_used: Foundation.NSNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var is_qualified: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var progress_percentage: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var qualified_date: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var time_configuration: Foundation.Data? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var trigger_configuration: Foundation.Data? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var trigger_type: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var trigger_value: Swift.String? {
    @objc get
    @objc set
  }
}
@_inheritsConvenienceInitializers @objc(ManagedClientConfig) public class ManagedClientConfig : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedClientConfig {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedClientConfig>
  @objc @NSManaged dynamic public var appName: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var clientId: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var clientToken: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var currencyCode: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var currencySymbol: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var settings: MobilePay.ManagedClientConfigSettings? {
    @objc get
    @objc set
  }
}
@_inheritsConvenienceInitializers @objc(ManagedClientConfigSettings) public class ManagedClientConfigSettings : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedClientConfigSettings {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedClientConfigSettings>
  @objc @NSManaged dynamic public var balanceEndpointOverride: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var help: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var loginEndpointOverride: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var preloadTopup: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var privacyPolicy: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var registrationType: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var showGiftCard: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var showGiveawayTokens: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var showMyOffers: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var showPrivacyPolicy: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var termsAndConditions: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var tokensEndpointOverride: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var tips: MobilePay.ManagedTips? {
    @objc get
    @objc set
  }
}
@_inheritsConvenienceInitializers @objc(ManagedComplimentaryBalance) public class ManagedComplimentaryBalance : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedComplimentaryBalance {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedComplimentaryBalance>
  @objc @NSManaged dynamic public var amount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var concessionaires: ObjectiveC.NSObject? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var details: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var expiryDate: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var id: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var slot: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var type: Swift.Int32 {
    @objc get
    @objc set
  }
}
@_inheritsConvenienceInitializers @objc(ManagedComplimentaryBalanceUsage) public class ManagedComplimentaryBalanceUsage : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedComplimentaryBalanceUsage {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedComplimentaryBalanceUsage>
  @objc @NSManaged dynamic public var amount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var charged: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var concessionaires: ObjectiveC.NSObject? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var expiryDate: Swift.String? {
    @objc get
    @objc set
  }
}
@_inheritsConvenienceInitializers @objc(ManagedCreditCard) public class ManagedCreditCard : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedCreditCard {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedCreditCard>
  @objc @NSManaged dynamic public var cardNumber: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var cardType: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var id: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var isDefault: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var paymentProfileSettings: Foundation.NSSet? {
    @objc get
    @objc set
  }
}
extension ManagedCreditCard {
  @objc(addPaymentProfileSettingsObject:) @NSManaged dynamic public func addToPaymentProfileSettings(_ value: MobilePay.ManagedPaymentProfileSettings)
  @objc(removePaymentProfileSettingsObject:) @NSManaged dynamic public func removeFromPaymentProfileSettings(_ value: MobilePay.ManagedPaymentProfileSettings)
  @objc(addPaymentProfileSettings:) @NSManaged dynamic public func addToPaymentProfileSettings(_ values: Foundation.NSSet)
  @objc(removePaymentProfileSettings:) @NSManaged dynamic public func removeFromPaymentProfileSettings(_ values: Foundation.NSSet)
}
@_inheritsConvenienceInitializers @objc(ManagedGiveawayToken) public class ManagedGiveawayToken : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedGiveawayToken {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedGiveawayToken>
  @objc @NSManaged dynamic public var expiryDate: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var id: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var name: Swift.String? {
    @objc get
    @objc set
  }
}
@_inheritsConvenienceInitializers @objc(ManagedPaymentProfileSettings) public class ManagedPaymentProfileSettings : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedPaymentProfileSettings {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedPaymentProfileSettings>
  @objc @NSManaged dynamic public var paymentProfileId: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var paymentProfileType: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
}
@_inheritsConvenienceInitializers @objc(ManagedTips) public class ManagedTips : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedTips {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedTips>
  @objc @NSManaged dynamic public var enabled: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var percentages: ObjectiveC.NSObject? {
    @objc get
    @objc set
  }
}
@_inheritsConvenienceInitializers @objc(ManagedTransaction) public class ManagedTransaction : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedTransaction {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedTransaction>
  @objc @NSManaged dynamic public var amount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var chargedAmount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var clientTransactionId: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var concessionaire: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var currency: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var date: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var discountAmount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var donationAmount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var event: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var taxAmount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var tip: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var tokenAmount: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var type: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var complimentaryBalanceUsage: Foundation.NSSet? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var items: Foundation.NSSet? {
    @objc get
    @objc set
  }
}
extension ManagedTransaction {
  @objc(addComplimentaryBalanceUsageObject:) @NSManaged dynamic public func addToComplimentaryBalanceUsage(_ value: MobilePay.ManagedComplimentaryBalanceUsage)
  @objc(removeComplimentaryBalanceUsageObject:) @NSManaged dynamic public func removeFromComplimentaryBalanceUsage(_ value: MobilePay.ManagedComplimentaryBalanceUsage)
  @objc(addComplimentaryBalanceUsage:) @NSManaged dynamic public func addToComplimentaryBalanceUsage(_ values: Foundation.NSSet)
  @objc(removeComplimentaryBalanceUsage:) @NSManaged dynamic public func removeFromComplimentaryBalanceUsage(_ values: Foundation.NSSet)
}
extension ManagedTransaction {
  @objc(addItemsObject:) @NSManaged dynamic public func addToItems(_ value: MobilePay.ManagedTransactionItem)
  @objc(removeItemsObject:) @NSManaged dynamic public func removeFromItems(_ value: MobilePay.ManagedTransactionItem)
  @objc(addItems:) @NSManaged dynamic public func addToItems(_ values: Foundation.NSSet)
  @objc(removeItems:) @NSManaged dynamic public func removeFromItems(_ values: Foundation.NSSet)
}
@_inheritsConvenienceInitializers @objc(ManagedTransactionItem) public class ManagedTransactionItem : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension ManagedTransactionItem {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<MobilePay.ManagedTransactionItem>
  @objc @NSManaged dynamic public var amount: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var name: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var quantity: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var sku: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var type: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var unitPrice: Foundation.NSDecimalNumber? {
    @objc get
    @objc set
  }
}
extension MobilePay.CreditCardScannerError.Kind : Swift.Equatable {}
extension MobilePay.CreditCardScannerError.Kind : Swift.Hashable {}
extension MobilePay.MobilePayEnvironment : Swift.Equatable {}
extension MobilePay.MobilePayEnvironment : Swift.Hashable {}
extension MobilePay.MobilePayEnvironment : Swift.RawRepresentable {}
